var SHEET_URL = "/sheet url here/"; //place your sheet link here
var SHEET_NAME = "/sheet name here/"; //place sheet name here

function main() {
    var sheet = SpreadsheetApp.openByUrl(SHEET_URL).getSheetByName(SHEET_NAME);
    var campaignIterator = AdWordsApp.campaigns().get();

    if (!sheet) {
        Logger.log('There is no sheet named '+SHEET_NAME+' in sheet url '+SHEET_URL);
        return;
    }
    sheet.clearContents();
    sheet.appendRow(['ID', 'Name', 'Status']);
    while (campaignIterator.hasNext()) {
        var campaign = campaignIterator.next();
        var campaignStatus = campaign.isEnabled() ? 'enabled' : campaign.isPaused() ? 'paused' : 'removed';
        sheet.appendRow([campaign.getId(), campaign.getName(), campaignStatus]);
    }
}